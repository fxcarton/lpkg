#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/syscall.h>
#include <dirent.h>
#include <pwd.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#ifndef __x86_64
#error architecture not supported
#endif
#if _POSIX_VERSION < 200809L
#error POSIX.1-2008 is required
#endif

static int check_pkgname(const char* pkgname) {
    const char* p;
    for (p = pkgname; (*p >= 'a' && *p <= 'z') || (*p >= 'A' && *p <= 'Z') || (*p >= '0' && *p <= '9') || *p == '_' || *p == '-'; p++);
    if (*p || p == pkgname) {
        fputs("Error: invalid pkgname\n", stderr);
        return 0;
    }
    return 1;
}

static FILE* get_pkg_file(const char* pkgname, const char* mode) {
    struct stat s;
    struct passwd* p;
    char* path;
    FILE* f;
    size_t n, m, t;
    if (!(p = getpwuid(getuid()))) {
        if (errno == ENOENT) errno = EACCES;
        return NULL;
    }
    n = strlen(p->pw_dir);
    m = strlen(pkgname);
    if (m >= ((size_t)-1) - n || (t = m + n) >= ((size_t)-8) || !(path = malloc(t + 8))) {
        errno = ENOMEM;
        return NULL;
    }
    memcpy(path, p->pw_dir, n);
    memcpy(path + n, "/.lpkg/", 8);
    if (mkdir(path, 0755) && errno != EEXIST) {
        free(path);
        errno = ENOENT;
        return NULL;
    }
    memcpy(path + n + 7, pkgname, m + 1);
    if (stat(path, &s) ? (errno != ENOENT) : (!S_ISREG(s.st_mode))) {
        errno = ENOENT;
        f = NULL;
    } else {
        f = fopen(path, mode);
    }
    free(path);
    return f;
}

static char* pkg_next_file(FILE* f) {
    unsigned char* p = NULL;
    size_t n = 0, a = 0;
    int c;
    while ((c = fgetc(f)) && c != EOF) {
        if (n >= ((size_t)-2)) {
            c = EOF;
        } else if (n + 1 < a) {
            p[n++] = c;
        } else {
            size_t na = (a < ((size_t)-128)) ? (a + 128) : ((size_t)-1);
            unsigned char* np;
            if (!(np = realloc(p, na))) {
                c = EOF;
            } else {
                p = np;
                a = na;
                p[n++] = c;
            }
        }
    }
    if (c == EOF) {
        free(p);
        p = NULL;
    } else if (p) {
        p[n] = 0;
    }
    return (char*)p;
}

static char* get_pkg_for_file_(const struct stat* spath) {
    struct passwd* p;
    char *base, *e;
    FILE* f;
    DIR* d;
    struct dirent* de;
    struct stat se;
    size_t n;
    if (!(p = getpwuid(getuid()))) return NULL;
    n = strlen(p->pw_dir);
    if (n >= ((size_t)-8) || !(base = malloc(n + 8))) return NULL;
    memcpy(base, p->pw_dir, n);
    memcpy(base + n, "/.lpkg/", 8);
    if ((mkdir(base, 0755) && errno != EEXIST)
     || !(d = opendir(base))) {
        free(base);
        return NULL;
    }
    free(base);
    errno = 0;
    while (!errno && (de = readdir(d))) {
        if ((f = get_pkg_file(de->d_name, "rb"))) {
            errno = 0;
            while ((e = pkg_next_file(f))) {
                if (lstat(e, &se)) {
                    free(e);
                    errno = 0;
                    continue;
                }
                free(e);
                if (se.st_dev == spath->st_dev && se.st_ino == spath->st_ino) {
                    if ((e = malloc(strlen(de->d_name) + 1))) {
                        strcpy(e, de->d_name);
                    }
                    fclose(f);
                    closedir(d);
                    return e;
                }
            }
            fclose(f);
        } else if (errno == ENOENT) {
            errno = 0;
        }
    }
    closedir(d);
    if (errno) {
        e = NULL;
    } else {
        if ((e = malloc(1))) *e = 0;
    }
    return e;
}

static char* get_pkg_for_file(const char* path) {
    struct stat spath;
    if (lstat(path, &spath)) {
        char* e;
        if ((e = malloc(1))) *e = 0;
        return e;
    }
    return get_pkg_for_file_(&spath);
}

static char* get_string(uint64_t ptr, pid_t pid) {
    unsigned char *s = NULL, *p;
    size_t n = 0, a = 0;
    long v;
    unsigned int align, r;
    for (;;) {
        errno = 0;
        align = ptr & (sizeof(long) - 1);
        ptr -= align;
        v = ptrace(PTRACE_PEEKDATA, pid, (void*)ptr, (void*)0);
        if (errno) {
            free(s);
            return NULL;
        }
        p = ((unsigned char*)&v) + align;
        for (r = sizeof(long) - align; r; r--) {
            if (n >= ((size_t)-1)) {
                free(s);
                return NULL;
            }
            if (n >= a) {
                unsigned char* new;
                a = (a < ((size_t)-64)) ? (a + 64) : ((size_t)-1);
                if (!(new = realloc(s, a))) {
                    free(s);
                    return NULL;
                }
                s = new;
            }
            if (!(s[n++] = *p++)) return (char*)s;
        }
        ptr += sizeof(long);
    }
    return NULL;
}

static char* resolvepathat(int dirfd, pid_t pid, const char* path) {
    char buffer[128], *p;
    struct stat s;
    size_t n, k = strlen(path);
    ssize_t m;
    if (!path) return NULL;
    if (*path != '/') {
        if (dirfd != AT_FDCWD) {
            sprintf(buffer, "/proc/%u/fd/%d", (unsigned int)pid, dirfd);
        } else {
            sprintf(buffer, "/proc/%u/cwd", (unsigned int)pid);
        }
        if (lstat(buffer, &s)) return NULL;
        if (s.st_size < 1024) s.st_size = 1024;
        if ((size_t)s.st_size >= ((size_t)-2) || ((n = (size_t)s.st_size + 1U) + 1U) >= ((size_t)-1) - k) return NULL;
        if (!(p = malloc(n + 1U + k))) return NULL;
        if ((m = readlink(buffer, p, n)) < 0 || ((size_t)m) >= n) {
            free(p);
            return NULL;
        }
        p[m] = '/';
        memcpy(p + m + 1U, path, k + 1U);
    } else {
        if (!(p = malloc(k + 1U))) return NULL;
        memcpy(p, path, k + 1U);
    }
    return p;
}

static int record_file(const char* pkgname, const char* path) {
    FILE* fpkg;
    size_t n;
    int r;
    if (!(fpkg = get_pkg_file(pkgname, "ab"))) {
        fprintf(stderr, "Error: failed to get package '%s' file\n", pkgname);
        return 0;
    }
    n = strlen(path) + 1;
    r = fwrite(path, 1, n, fpkg) == n;
    if (fclose(fpkg)) r = 0;
    if (!r) fputs("Error: failed to record new path in package file\n", stderr);
    return r;
}

static int check_path(uint64_t path, int dirfd, int noFollowSymlink, int delete, pid_t child, const char* pkgname) {
    struct stat spath;
    char *p = get_string(path, child), *fp;
    int r = 0;
    if (!p) {
        fprintf(stderr, "Error: failed to retrieve path string from child process\n");
        return 0;
    }
    if (!(fp = resolvepathat(dirfd, child, p))) {
        fprintf(stderr, "Error: failed to resolve path '%s' from dir %d in process %d\n", p, dirfd, (int)child);
        free(p);
        return 0;
    }
    free(p);
    if ((noFollowSymlink ? lstat : stat)(fp, &spath)) {
        if (errno != ENOENT) {
            fprintf(stderr, "Error: failed to stat path\n");
        } else if (delete) {
            r = 1;
        } else {
            r = record_file(pkgname, fp);
        }
    } else {
        char* pkg;
        if (!(pkg = get_pkg_for_file_(&spath))) {
            fprintf(stderr, "Error: reading package files\n");
        } else if (!(r = !strcmp(pkg, pkgname) || !*pkg)) {
            fprintf(stderr, "Error: file belongs to package %s, access denied\n", pkg);
        } else if (!*pkg && S_ISREG(spath.st_mode)) {
            r = record_file(pkgname, fp);
        }
        free(pkg);
    }
    free(fp);
    return r;
}

#define OPEN_FLAGS_WRITE(flags) (((flags) & O_WRONLY) == O_WRONLY || ((flags) & O_RDWR) == O_RDWR)
static int check_syscall(uint64_t syscall, const uint64_t* args, pid_t child, const char* pkgname) {
    switch (syscall) {
        case SYS_chmod:     return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_chown:     return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_creat:     return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_fchmodat:  return check_path(args[1], args[0], args[2] & AT_SYMLINK_NOFOLLOW, 0, child, pkgname);
        case SYS_fchownat:  return check_path(args[1], args[0], args[2] & AT_SYMLINK_NOFOLLOW, 0, child, pkgname);
        case SYS_futimesat: return check_path(args[1], args[0], 0, 0, child, pkgname);
        case SYS_lchown:    return check_path(args[0], AT_FDCWD, 1, 0, child, pkgname);
        case SYS_link:      return check_path(args[1], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_linkat:    return check_path(args[3], args[2], 0, 0, child, pkgname);
        case SYS_mkdir:     return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_mkdirat:   return check_path(args[1], args[0], 0, 0, child, pkgname);
        case SYS_mknod:     return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_mknodat:   return check_path(args[1], args[0], 0, 0, child, pkgname);
        case SYS_rename:    return check_path(args[0], AT_FDCWD, 1, 1, child, pkgname) && check_path(args[1], AT_FDCWD, 1, 0, child, pkgname);
        case SYS_renameat:  return check_path(args[1], args[0], 1, 1, child, pkgname) && check_path(args[3], args[2], 1, 0, child, pkgname);
        case SYS_rmdir:     return check_path(args[0], AT_FDCWD, 1, 1, child, pkgname);
        case SYS_symlink:   return check_path(args[1], AT_FDCWD, 1, 0, child, pkgname);
        case SYS_symlinkat: return check_path(args[2], args[1], 1, 0, child, pkgname);
        case SYS_truncate:  return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_unlink:    return check_path(args[0], AT_FDCWD, 1, 1, child, pkgname);
        case SYS_unlinkat:  return check_path(args[1], args[0], 1, 1, child, pkgname);
        case SYS_utime:     return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_utimes:    return check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_utimensat: return check_path(args[1], args[0], args[2] & AT_SYMLINK_NOFOLLOW, 0, child, pkgname);
        case SYS_open:      return !OPEN_FLAGS_WRITE(args[1]) || check_path(args[0], AT_FDCWD, 0, 0, child, pkgname);
        case SYS_openat:    return !OPEN_FLAGS_WRITE(args[2]) || check_path(args[1], args[0], 0, 0, child, pkgname);
    }
    return 1;
}

static int ptrace_is_user_signal(pid_t pid) {
    siginfo_t siginfo;
    if (ptrace(PTRACE_GETSIGINFO, pid, (void*)0, &siginfo)) {
        fprintf(stderr, "Warning: failed to get signal info\n");
        return 0;
    }
    return siginfo.si_code <= 0;
}

static int lpkg_install(int argc, char** argv) {
    struct user_regs_struct registers;
    char* defaultcmd[] = {"make", "install", NULL};
    const char* pkgname;
    pid_t inSyscall[16], child, pid;
    unsigned long retval = 0;
    unsigned int i, numInSyscall = 0;
    int status;

    if (argc < 1) {
        fputs("Usage: lpkg install <pkgname> [command [cmdargs...]]\n", stderr);
        return 1;
    }
    if (!check_pkgname(pkgname = argv[0])) return 1;
    if (argc == 1) {
        argc = 2;
        argv = defaultcmd;
    } else {
        argc--;
        argv++;
    }
    if ((child = fork()) == -1) {
        fprintf(stderr, "Error: failed to fork, %s\n", strerror(errno));
        return 1;
    }
    if (!child) {
        if (ptrace(PTRACE_TRACEME, 0, (void*)0, (void*)0)) {
            fprintf(stderr, "Error: failed to trace command, %s\n", strerror(errno));
        } else {
            execvp(argv[0], argv);
            fprintf(stderr, "Error: failed to exec command, %s\n", strerror(errno));
        }
        return 1;
    }
    if (waitpid(child, &status, __WALL) != child || !WIFSTOPPED(status)) {
        fputs("Error: failed to trace child\n", stderr);
        kill(child, SIGKILL);
        return 1;
    }
    if (ptrace(PTRACE_SETOPTIONS, child, (void*)0, PTRACE_O_EXITKILL | PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK | PTRACE_O_TRACECLONE | PTRACE_O_TRACEEXIT | PTRACE_O_TRACESYSGOOD | PTRACE_O_TRACEEXEC)) {
        fputs("Error: failed to jail child\n", stderr);
        kill(child, SIGKILL);
        return 1;
    }
    if (ptrace(PTRACE_SYSCALL, child, (void*)0, (void*)0)) {
        fprintf(stderr, "Error: failed to step to next syscall, %s\n", strerror(errno));
        return 1;
    }
    for (;;) {
        switch ((pid = waitpid(-1, &status, __WALL))) {
            case 0: continue;
            case -1: fprintf(stderr, "Error: failed to wait for next event, %s\n", strerror(errno)); return 1;
        }
        if (WIFEXITED(status) || WIFSIGNALED(status)) {
            if (pid == child) return (WIFEXITED(status)) ? (WEXITSTATUS(status)) : (WTERMSIG(status));
            continue;
        }
        if (!WIFSTOPPED(status)) {
            fputs("Error: child process is not stopped\n", stderr);
            return 1;
        }
        if ((WSTOPSIG(status) != SIGTRAP && WSTOPSIG(status) != (SIGTRAP | 0x80)) || ptrace_is_user_signal(pid)) {
            if (ptrace(PTRACE_SYSCALL, pid, (void*)0, WSTOPSIG(status))) {
                fprintf(stderr, "Error: failed to step to next syscall, %s\n", strerror(errno));
                return 1;
            }
            continue;
        }
        switch (status >> 8) {
            case (SIGTRAP | (PTRACE_EVENT_FORK << 8)):
            case (SIGTRAP | (PTRACE_EVENT_VFORK << 8)):
            case (SIGTRAP | (PTRACE_EVENT_CLONE << 8)):
                {
                    unsigned long newpid;
                    if (ptrace(PTRACE_GETEVENTMSG, pid, (void*)0, &newpid)) {
                        fprintf(stderr, "Error: failed to get new pid, %s\n", strerror(errno));
                        return 1;
                    }
                    if (ptrace(PTRACE_SYSCALL, pid, (void*)0, (void*)0)) {
                        fprintf(stderr, "Error: failed to step to next syscall, %s\n", strerror(errno));
                        return 1;
                    }
                }
                continue;
            case (SIGTRAP | (PTRACE_EVENT_EXIT << 8)):
                if (pid == child) {
                    if (ptrace(PTRACE_GETEVENTMSG, pid, (void*)0, &retval)) {
                        retval = 0;
                    } else {
                        retval = WEXITSTATUS(retval);
                    }
                }
                for (i = 0; i < numInSyscall; i++) {
                    if (inSyscall[i] == pid) {
                        inSyscall[i] = inSyscall[--numInSyscall];
                    }
                }
                if (ptrace(PTRACE_SYSCALL, pid, (void*)0, (void*)0)) {
                    fprintf(stderr, "Error: failed to step to next syscall, %s\n", strerror(errno));
                    return 1;
                }
                continue;
            case (SIGTRAP | (PTRACE_EVENT_EXEC << 8)):
                {
                    unsigned long oldpid;
                    if (!ptrace(PTRACE_GETEVENTMSG, pid, (void*)0, &oldpid)) {
                        for (i = 0; i < numInSyscall; i++) {
                            if (inSyscall[i] == oldpid) {
                                inSyscall[i] = pid;
                            }
                        }
                    }
                    if (ptrace(PTRACE_SYSCALL, pid, (void*)0, (void*)0)) {
                        fprintf(stderr, "Error: failed to step to next syscall, %s\n", strerror(errno));
                        return 1;
                    }
                }
                continue;
            case (SIGTRAP | 0x80):
                for (i = 0; i < numInSyscall; i++) {
                    if (pid == inSyscall[i]) break;
                }
                if (i < numInSyscall) { /* syscall exit */
                    inSyscall[i] = inSyscall[--numInSyscall];
                } else { /* syscall enter */
                    uint64_t args[6];
                    if (numInSyscall >= (sizeof(inSyscall) / sizeof(*inSyscall))) {
                        fprintf(stderr, "Error: too much processes in syscalls\n");
                        return 1;
                    }
                    inSyscall[numInSyscall++] = pid;
                    if (ptrace(PTRACE_GETREGS, pid, (void*)0, &registers)) {
                        fprintf(stderr, "Error: failed to get child registers, %s\n", strerror(errno));
                        return 1;
                    }
                    args[0] = registers.rdi;
                    args[1] = registers.rsi;
                    args[2] = registers.rdx;
                    args[3] = registers.r10;
                    args[4] = registers.r8;
                    args[5] = registers.r9;
                    if (!check_syscall(registers.orig_rax, args, pid, pkgname)) return 1;
                }
                if (ptrace(PTRACE_SYSCALL, pid, (void*)0, (void*)0)) {
                    fprintf(stderr, "Error: failed to step to next syscall, %s\n", strerror(errno));
                    return 1;
                }
                continue;
        }
        fputs("Error: unhandled ptrace situation\n", stderr);
        return 1;
    }
    return 1;
}

static int lpkg_list(int argc, char** argv) {
    FILE* f;
    char* p;
    if (argc != 1) {
        fputs("Usage: lpkg list <pkgname>\n", stderr);
        return 1;
    }
    if (!check_pkgname(argv[0])) return 1;
    if (!(f = get_pkg_file(argv[0], "rb"))) {
        if (errno == ENOENT) {
            fprintf(stderr, "Error: package '%s' is not installed\n", argv[0]);
        } else {
            fprintf(stderr, "Error: failed to get package '%s' file\n", argv[0]);
        }
        return 1;
    }
    errno = 0;
    while ((p = pkg_next_file(f))) {
        fputs(p, stdout);
        fputc('\n', stdout);
        free(p);
    }
    if (errno) fprintf(stderr, "Error: while reading file list for package '%s', %s\n", argv[0], strerror(errno));
    fclose(f);
    return 0;
}

static int lpkg_uninstall(int argc, char** argv) {
    FILE* f;
    char* p;
    int ok = 1;
    if (argc != 1) {
        fputs("Usage: lpkg uninstall <pkgname>\n", stderr);
        return 1;
    }
    if (!check_pkgname(argv[0])) return 1;
    if (!(f = get_pkg_file(argv[0], "rb+"))) {
        if (errno == ENOENT) {
            fprintf(stderr, "Error: package '%s' is not installed\n", argv[0]);
        } else {
            fprintf(stderr, "Error: failed to get package '%s' file\n", argv[0]);
        }
        return 1;
    }
    errno = 0;
    while ((p = pkg_next_file(f))) {
        fputs("rm ", stdout);
        fputs(p, stdout);
        fputc('\n', stdout);
        if (remove(p) && errno != ENOENT && errno != ENOTEMPTY && errno != EEXIST) {
            fputs("Warning: failed to remove file\n", stderr);
            ok = 0;
        }
        free(p);
        errno = 0;
    }
    if (errno) {
        ok = 0;
        fprintf(stderr, "Error: while reading file list for package '%s', %s\n", argv[0], strerror(errno));
    }
    if (ok) {
        if (ftruncate(fileno(f), 0)) fputs("Warning: failed to clear pkg file\n", stderr);
    }
    fclose(f);
    return !ok;
}

static int lpkg_belongs(int argc, char** argv) {
    const char* path;
    char* pkg;
    while (argc > 0) {
        argc--;
        path = *argv++;
        fputs(path, stdout);
        fputs(": ", stdout);
        if ((pkg = get_pkg_for_file(path))) {
            fputs((*pkg) ? pkg : "not owned by any package", stdout);
            fputc('\n', stdout);
            free(pkg);
        } else {
            fputs("error searching packages\n", stdout);
            return 1;
        }
    }
    return 0;
}

int main(int argc, char** argv) {
    int (*command)(int argc, char** argv);

    if (argc < 3) {
        fputs("Usage: lpkg install <pkgname> [command [cmdargs...]]\n"
              "    or lpkg list|uninstall <pkgname>\n"
              "    or lpkg belongs <paths>\n", stderr);
        return 1;
    }

    if (!strcmp(argv[1], "install")) {
        command = lpkg_install;
    } else if (!strcmp(argv[1], "list")) {
        command = lpkg_list;
    } else if (!strcmp(argv[1], "uninstall")) {
        command = lpkg_uninstall;
    } else if (!strcmp(argv[1], "belongs")) {
        command = lpkg_belongs;
    } else {
        fputs("Error: invalid command\n", stderr);
        return 1;
    }

    return command(argc - 2, argv + 2);
}
