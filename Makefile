CFLAGS ?= -std=c89 -pedantic -D_POSIX_C_SOURCE=200809L -Wall -g -march=native
PREFIX ?= $(HOME)/.local
BINDIR ?= bin
MANDIR ?= share/man

lpkg: lpkg.o

.PHONY: clean
clean:
	rm -f lpkg.o lpkg

.PHONY: install
D := $(if $(DESTDIR),$(DESTDIR)/)$(PREFIX)
install: lpkg
	mkdir -p $(D)/$(BINDIR) $(D)/$(MANDIR)/man1
	cp lpkg $(D)/$(BINDIR)
	cp lpkg.1 $(D)/$(MANDIR)/man1

.PHONY: lpkg_install
lpkg_install: lpkg
	./lpkg install lpkg $(MAKE) install
